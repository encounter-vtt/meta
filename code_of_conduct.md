# EncounterVTT Code of Conduct
EncounterVTT is commited to nurturing a friendly, safe, and welcoming community for everyone. For that goal, we hold everyone to a set of standards.

## Expected behaviour
In addition to behaviours we do not permit, we desire that everyone attempts to exemplify other behaviours:
- **Encourage growth.** Everyone starts somewhere, and everyone can grow. We help our peers improve.
- **Benefit of the doubt.** Until something negative is confirmed or compounded to the point where it's fairly certain, avoid assumptions, use questions.
- **Be mindful of the other person.** We are all sentient beings and sometimes we bring baggage. Sometimes space is the best answer.
- **Seek out help.** Within our community, there are people who take on various responsibilities or may simply have life experiences making them well-suited to handle something. This especially applies to moderation and mediation.
- **Tag content appropriately.** Content that might be contextually inappropriate to cross someone's screen should be marked with warnings in a place that appears before it is viewed.

## Prohibited behaviours
This is a non-exhaustive list. It is meant to provide a near-solid picture of what kind of behaviours we will prohibit.
- **Discrimination.** We expect our community to be free of all discrimination, whether it derives from age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.
- **Harassment.** Behaviours that serve to directly undermine another member or make them feel unsafe, whether intentional or not, are considered harassment.
- **Verbally unwelcome behaviours.** When asked to stop doing something, there are exactly three acceptable responses: apologize, ask for clarification, or nothing at all.

## Tolerance Paradox
The Tolerance Paradox describes the scenario of tolerating intolerant behaviour. We hold to that tolerating intolerance is imposing that intolerance on our community, and is as bad, if not worse, as practicing it ourselves. We will not tolerate intolerant behaviour.

## Moderator Action
Moderators will prefer mediation over action, but that's not always a luxury to be had. Moderators will pick from one of the following actions, applying them in order, but they may start from any point, skipping as deemed necessary for the behaviours they are responding to. Actions are cross-venue. Moderators may de-escalate statuses and repeal actions after review. Actions will be logged publicly, though reasons may be withheld, substituted with an explanation for why it was withheld.
- Up to 2 Formal warnings
- 1-4 hour mute
- 1-3 day mute
- 1-4 week tempban
- Permanent ban
