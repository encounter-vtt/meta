# RFC - Ready For Comment
#### Design, Discuss, Repeat until we're happy.

## When and Why
The RFC process is to be used for defining features and other large changes for Encounter VTT. It will give people with a stake in Encounter a chance to give their input on major decisions relating to the usability of the platform. This process is loosely adopted from [the Rust Programming Language](https://github.com/rust-lang/rfcs).

Things that don't need RFCs:
- Internal changes that don't reach any API or UX.
- Changes to API that require, for example, less than 10 LOC of migration changes.
- Changes to UX that wouldn't require a user to read about what changed in order to continue using the platform.

If in doubt, as in the community whether something is worth an RFC or not.

## What
An RFC needs the following:
- An overview of the problem being solved.
- The proposed solution, with rough implementation details.
- Alternatives considered and why they were discounted in favour of the proposed solution.
- Drawbacks to the proposed solution.

## How
1) Pre-RFC discussion: Frame your idea as a problem + solution, then get it polished with the community. While this isn't mandatory, it's fairly important in order to ensure your idea is communicated effectively.  
2) Fork the RFC repo  
3) Copy and fill the template: Copy the `0000-template.md` file into the `text` directory, renaming it from `0000-template.md` to `0000-my-feature.md`.  
    - Make sure to leave the numbers alone, as it will be set prior to merge, if accepted.  
    - Fill in the RFC. Put care into the details, the design should be persuasive.  
4) Submit a PR: This signals the community that the RFC is Ready For Comment.  

## Review and Acceptance
RFCs that don't have a reason for immediate rejection will be left open for a minimum of 2 weeks, up to 6 months. If the development team decides that an RFC has had all the feedback it could have in those 6 months, they will decide, based on that feedback, whether to merge it or not.

Reasons for immediate rejection include:
- Incomplete or otherwise doesn't convey clearly enough
- Impossible without violating project standards
- Something in it violates our Code of Conduct

## Revision
RFCs cannot be deleted or edited (aside from grammatical or other changes that don't affect meaning). Concepts proposed in previous RFCs can only be revised or altered through another RFC.

# Pre-release Note
While Encounter is still in early stages, the developers can and probably will be adding things without a proper RFC. This is necessary to get the project off the ground. Decisions made in this fashion are open to revision via RFCs.
