# Table of Contents
| File | Description |
| --- | ---: |
| [`RFCs`](rfc.md) | The process for which large bigger-picture changes are made to the platform |