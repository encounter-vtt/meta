# Project Meta

Documents, including policies, procedures, standards, and templates, for the overarching project.

Unless otherwise stated, all files here are for transparency only, all rights reserved.

## Directory
| Folder | Description |
| --- | ---:  |
| `code_of_conduct.md` | The Code of Conduct for participation in the community |
| `inspirational.md` | A collection of thoughts and ideas that the project was started with. Over time, portions will be turned into RFCs and implemented. |
| `team.md` | The team directory |
| `dev/templates/` | Templates for Cargo, Rustfmt, and other configurations |
| `dev/new-project.md` | Instructions for properly configuring a new project, including dev ops |
| `dev-procedures/` | Various procedures for the development cycle. |
| `dev-procedures/index.md` | Table of Contents for this directory. |
| `media/` | All media that belongs to the project. |
| `media/index.md` | Index of all media, with meta data, including licenses. |
